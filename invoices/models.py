from __future__ import division
from decimal import Decimal
from hashlib import md5

from django.db import models
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib.auth.models import User
from django.db.models import Sum
from django.core.urlresolvers import reverse

from invoices.signals import invoice_ready

class Invoice(models.Model):
    """
    Invoice

    """
    coach = models.ForeignKey(User, blank=False, null=False, related_name='invoices')
    client = models.ForeignKey(User, blank=False, null=False, related_name='invoices_for')
    created = models.DateTimeField(verbose_name=_('Created'), auto_now_add=True)
    updated = models.DateTimeField(verbose_name=_('Updated'), auto_now=True)
    begins = models.DateField(verbose_name=_('Begin'))
    ends = models.DateField(verbose_name=_('End'))
    due_date = models.DateField(verbose_name=_('Due date'), null=True, blank=True)
    is_paid = models.BooleanField(verbose_name=_('Is paid'), default=False)
    currency = models.CharField(max_length=3, default='EUR')
    amount = models.DecimalField(max_digits=7, decimal_places=2, verbose_name=_('Total amount'), default=Decimal("0.0"), help_text=_('Without VAT'))
    vat_amount = models.DecimalField(max_digits=7, decimal_places=2, verbose_name=_('Total amount'), default=Decimal("0.0"))
    total_amount = models.DecimalField(max_digits=7, decimal_places=2, verbose_name=_('Total amount'), default=Decimal("0.0"), help_text=_('Including VAT'))
    vat = models.PositiveIntegerField(verbose_name=_('VAT'), default=19)
    cancels = models.OneToOneField("Invoice", blank=True, null=True)

    @property
    def number(self):
        """
        Invoice number of format "YYYYMM0001"

        """
        r = md5(unicode(self.id)).hexdigest()[:8]
        return '%d-%s-%s' % (self.begins.year, unicode(self.begins.month).zfill(2), unicode(r).zfill(8))

    def calculate(self, silent=False):

        if self.cancels:
            # cancellation of existing invoice
            for item in self.cancels.line_items.all():
                i = Item.objects.create(invoice=self, name=item.name, total_amount = -item.total_amount)
                self.amount += i.total_amount
                for item_group in item.line_item_groups.all():
                    LineItemGroup.objects.create(item=i, item_type=item_group.item_type, amount=-item_group.amount, description=item_group.description)
        else:
            # regular invoice
            for item in self.line_items.all():
                total_amount 
                self.amount += total_amount

        # add vat
        self.vat_amount = self.amount * Decimal(self.vat / 100).quantize(Decimal('1.00'))
        self.total_amount = self.amount + self.vat_amount
        self.save()

        if not silent:
            invoice_ready.send(sender=self, invoice=self)
        return self

    def get_absolute_url(self):
        return reverse('invoice_detail', kwargs={'pk': self.pk})

    def __unicode__(self):
        return "%s, %s - %s" % (self.coach, self.begins, self.ends)

    class Meta:
        ordering = ['-begins', '-ends',]

class LineItemType(models.Model):
    """
    Line item type

    """
    coach = models.ForeignKey(User, related_name='line_item_types')
    identifier = models.CharField(max_length=128)
    name = models.CharField(max_length=256)
    description = models.CharField(max_length=512, blank=True)
    amount = models.DecimalField(max_digits=7,
        decimal_places=2, verbose_name=_('Amount'), default=Decimal("0.0"))


    def __unicode__(self):
        return u'%s' % self.identifier

class LineItem(models.Model):
    """
    Line item

    """
    invoice = models.ForeignKey(Invoice, related_name='line_items')
    item_type = models.ForeignKey(LineItemType, related_name='line_item_types')
    description = models.CharField(max_length=512, verbose_name=_('Description'))
    quantity = models.IntegerField(verbose_name=_('Quantity'))
    date = models.DateTimeField(verbose_name=_('Date'))
    
    def __unicode__(self):
        return self.description

    @property
    def subtotal(self):
        return self.quantity * self.item_type.amount