from django.contrib import admin
from django.utils.translation import ugettext, ungettext, ugettext_lazy as _

from .models import Invoice, LineItem, LineItemType
from .forms import InvoiceForm
from invoices import cancel_invoice

class InvoiceAdmin(admin.ModelAdmin):
    pass
    # form = InvoiceForm
    # readonly_fields = ('total_amount',)
    # list_display = ('coach', 'begins', 'ends', 'total_amount', 'is_paid')
    # list_filter = ('is_paid',)
    # actions = ['cancel_invoices']

    # def cancel_invoices(self, request, queryset):

    #     for invoice in queryset:
    #         cancel_invoice(invoice)

    #     message = ungettext("successfully cancelled %(count)d invoice",
    #         "successfully cancelled %(count)d invoices", queryset.count()) % {'count': queryset.count()}
    #     self.message_user(request, message)
    # cancel_invoices.short_description = _('Cancel selected invoices')


class LineItemAdmin(admin.ModelAdmin):
    pass

class LineItemTypeAdmin(admin.ModelAdmin):
    pass

admin.site.register(Invoice, InvoiceAdmin)
admin.site.register(LineItem, LineItemAdmin)
admin.site.register(LineItemType, LineItemTypeAdmin)
