from django.shortcuts import get_object_or_404, render_to_response
from django.template import RequestContext
from django.views.generic import ListView, DetailView

from invoices.models import Invoice

class InvoiceListView(ListView):
    model = Invoice
    context_object_name = 'invoice_list'

    def get_queryset(self):
        return Invoice.objects.all()

class InvoiceDetailView(DetailView):
    model = Invoice
    context_object_name = 'invoice'

    def get_object(self):
        invoice = get_object_or_404(Invoice, pk=self.kwargs['pk'])
        return invoice
    	
def full_invoice(request, pk):
    invoice = get_object_or_404(Invoice, pk=pk)
    line_items = invoice.line_items.all

    ctx = RequestContext(request, {
        'invoice': invoice,
        'line_items': line_items,
        })
    return render_to_response('invoices/full_invoice.html', ctx)
